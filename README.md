# Tactos-map


Tactos-map is a web base application designed to display verctor map. It's written using [openlayers](http://openlayers.org) library.

### features

- speech synthesis of street name on speech.
- create checkpoint with `w` key and move map to get checkpoint under the mouse pointer with `c`

## Build

This project use [parcels](https://parceljs.org) to build the application and get vector tiles from a server serving WMS tiles at the following address `http://localhost:8080/data/v3/{z}/{x}/{y}.pbf`.
You can use the [tileserver-gl-light](https://tileserver.readthedocs.io) configuration located in **tileserver** folder.

- install `node 10`
- Download some mbtile file to serve it with tileserver.
- Modify the [config.json](tileserver/config.json) accordingly
- Modify [main.js](main.js) to point to right location
- run `npm install --also=dev`

### Run

- run `npm dev`
- open browser at `localhost:1234`
- tileserver is available at port 8080

## Contact

This project is developed by the the laboratory of [Costech](http://www.costech.utc.fr/) in the [University of Technology of Compiegne (UTC)](https://www.utc.fr/) in partnership with [hypra](http://hypra.fr/-Accueil-1-.html) and [natbraille](http://natbraille.free.fr/).
