export function createMapboxStreetsV6Style(Style, Fill, Stroke, Icon, Text) {
  var fill = new Fill({color: ''});
  var stroke = new Stroke({color: '', width: 1});
  var polygon = new Style({fill: fill});
  var strokedPolygon = new Style({fill: fill, stroke: stroke});
  var line = new Style({stroke: stroke});
  var text = new Style({text: new Text({
    text: '', fill: fill, stroke: stroke
  })});
  var styles = [];
  return function(feature, resolution) {
    var length = 0;
    var layer = feature.get('layer');
    var cls = feature.get('class');
    var type = feature.get('type');
    var geom = feature.getGeometry().getType();
if (layer == 'landuse'){
    fill.setColor('#e0e4dd');
    styles[length++] = polygon;
} else if (layer == 'water'){
  fill.setColor('#a0c8f0');
  styles[length++] = polygon;
} else if (layer == 'waterway') {
  stroke.setColor('#00f');
  stroke.setWidth(1);
  styles[length++] = line;
} else if (layer == 'landcover') {
  fill.setColor('#f0e8f8');
  styles[length++] = polygon;
} else if (layer == 'park') {
  fill.setColor('rgb(50,238,50)');
  styles[length++] = polygon;
} else if (layer == 'boundary') {
  fill.setColor('rgb(200,238,200)');
  styles[length++] = polygon;
} else if (layer == 'aeroway') {
  fill.setColor('rgb(200,238,50)');
  styles[length++] = polygon;
} else if (layer == 'transportation') {
  stroke.setColor('#bbb');
  stroke.setWidth(1);
  styles[length++] = line;
} else if (layer == 'building') {
  fill.setColor('#f2eae2');
  stroke.setColor('#dfdbd7');
  stroke.setWidth(1);
  styles[length++] = strokedPolygon;
} else if (layer == 'water_name') {
  fill.setColor('#74aee9');
  stroke.setColor('rgba(255,255,255,0.8)');
  stroke.setWidth(1);
  styles[length++] = text;
} else if (layer == 'transportation_name') {
  stroke.setColor('#bbb');
  stroke.setWidth(2);
  styles[length++] = line;
} else if (layer == 'place') {
  fill.setColor('#333');
  stroke.setColor('rgba(255,255,255,0.8)');
  stroke.setWidth(1);
  styles[length++] = text;
} else if (layer == 'housenumber') {
  fill.setColor('#633');
  stroke.setColor('rgba(255,255,255,0.8)');
  stroke.setWidth(1);
  styles[length++] = text;
} else if (layer == 'poi') {
  fill.setColor('#633');
  stroke.setColor('rgba(255,255,255,0.8)');
  stroke.setWidth(1);
  styles[length++] = text;
}
styles.length = length;
return styles;
};
}
