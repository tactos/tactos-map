import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import GeoJSON from 'ol/format/GeoJSON';
import {Fill, Stroke, Style, Circle} from 'ol/style';

function style(feature, resolution) {
  var geometryType = feature.getGeometry().getType();
  var properties = feature.get('_umap_options') ?? feature.getProperties();
  switch (geometryType) {
    case 'Polygon':
      return new Style({ fill: new Fill({ color : properties.fillColor})});
      break;
      case 'Point':
        return new Style({ image: new Circle({ radius: 5,
                                                fill: new Fill({color :properties.color}),
                                              }),
                          });
        break;
      case 'LineString':
        return new Style({stroke: new Stroke({ color: properties.color, width: 2})})
        break;
    default:
  }

}

var source = new VectorSource({
  features: new GeoJSON().readFeatures({
    "type": "FeatureCollection",
    "features": []}),
  })

var layer = new VectorLayer({
    source: source,
    style: style
  });

export function updateLayer(fileElem, map) {
  map.addLayer(layer);
  fileElem.addEventListener('change', function (event) {
    var reader = new FileReader();
    reader.onload = function (e) {
      var content = e.target.result;
      source.clear();
      source.addFeatures(new GeoJSON().readFeatures(content));
    }
    reader.readAsText(event.target.files[0])
  })
}
