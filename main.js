import 'ol/ol.css';
import MVT from 'ol/format/MVT';
import Map from 'ol/Map';
import TileGrid from 'ol/tilegrid/TileGrid';
import VectorTileLayer from 'ol/layer/VectorTile';
import VectorTileSource from 'ol/source/VectorTile';
import View from 'ol/View';
import {Fill, Icon, Stroke, Style, Text, Circle} from 'ol/style';
import {fromLonLat} from 'ol/proj';
import {get as getProjection} from 'ol/proj';
import { createMapboxStreetsV6Style } from './js/style.js';
import {useGeographic} from 'ol/proj';
import {updateLayer} from './js/geojson.js'
useGeographic();

// longitude, latitude initial location
var initial_location = [2.8302, 49.419];

var resolutions = [];
  for (var i = 0; i <= 15; ++i) {
    resolutions.push(156543.03392804097 / Math.pow(2, i));
  }

function tileUrlFunction(tileCoord) {
  return ('http://localhost:8080/data/v3/{z}/{x}/{y}.pbf'
)
  .replace('{z}', String(tileCoord[0]-1))
  .replace('{x}', String(tileCoord[1]))
  .replace('{y}', String(tileCoord[2]));
  }

var view = new View({
  center:  initial_location,
  minZoom: 1,
  zoom: 13,
});


var map = new Map({
  layers: [
    new VectorTileLayer({
      source: new VectorTileSource({
        format: new MVT(),
        tileGrid: new TileGrid({
          extent: getProjection('EPSG:3857').getExtent(),
          resolutions: resolutions,
          tileSize: 512,
        }),
        tileUrlFunction: tileUrlFunction,
        }),
      style: createMapboxStreetsV6Style(Style, Fill, Stroke, Icon, Text),
    })
  ],
  target: 'map',
  view: view,
});

updateLayer(document.getElementById('fileElem'), map);
var mouse_position;
var savedCheckpoint;

document.addEventListener("keydown", function (event) {
	switch(String.fromCharCode( event.keyCode ) ){
		case 'w' :
		case 'W' :
      console.log('saving checkpoint on ' + mouse_position);
      savedCheckpoint = mouse_position;
		break;

		case 'x' :
		case 'X' :
      var center = [];
      center[0] = savedCheckpoint[0] - mouse_position[0];
      center[1] = savedCheckpoint[1] - mouse_position[1];
      view.adjustCenter(center);
		break;
	}
});

map.on('pointermove', (event)=> {
  mouse_position = event.coordinate;
});
map.on('click', sayInfo);

var speech = window.speechSynthesis;

var info = document.getElementById('info');
function sayInfo(event) {
  // look for features around mouse pointer with radius of 16
  var features = map.getFeaturesAtPixel(event.pixel, {hitTolerance : 16});
  // display extra text information
  if (features.length == 0) {
    info.innerText = '';
    info.style.opacity = 0;
    return;
  }
  info.innerText = JSON.stringify(properties, null, 2);
  info.style.opacity = 1;
  var properties = features[0].getProperties();
  features.forEach((el) => {
    if (el.getProperties().name)
    {
      info.innerText = el.getProperties().name;
      var ut = new SpeechSynthesisUtterance(el.getProperties().name)
      ut.lang = 'fr';
      if(!speech.pending & !speech.speaking )
      {
        speech.speak(ut);
      }
      return;
    }
  });
}
